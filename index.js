var mangasites = Object.create(null);
mangasites["http://www.mangareader.net/"] = {
    extract: function(title) {
        var regex = /^ *((.*) +([0-9]+)).*- +Read +\1 +Online +- +Page +(\d+) *$/i;
        var match = regex.exec(title);
        if (!match)
            return false;
        return {
            name: match[2],
            current: parseInt(match[3], 10),
            pagenum: parseInt(match[4], 10)
        };
    },
    completed_test: function(html) {
        return html.indexOf("is marked as completed") != -1;
    },
    released_test: function(html) {
        return html.indexOf("is not released yet") == -1;
    }
};
mangasites["http://mangafox.me/"] = {
    extract: function(title) {
        var regex = /^ *((.*) +([.0-9]+)).*- +Read +\1 +Online +- +Page +(\d+) *$/i;
        var match = regex.exec(title);
        if (!match)
            return false;
        return {
            name: match[2],
            current: parseFloat(match[3]),
            pagenum: parseInt(match[4], 10)
        };
    },
    completed_test: function(html) {
        return /Next Chapter: *<\/span> *Completed/i.test(html);
    },
    released_test: function(html) {
        return !/Next Chapter:(.(?!<\/p>))*is coming next/i.test(html);
    }
};

var data = require("sdk/self").data;
var { setTimeout } = require("sdk/timers");
var request = require("sdk/request");
var history = require("sdk/places/history");
var tabs = require("sdk/tabs");
var windows = require("sdk/windows").browserWindows;
var { viewFor } = require("sdk/view/core");

var mangas = require("sdk/panel").Panel({
  width: 300,
  height: 450,
  contentURL: data.url("manga-list.html"),
  onHide: handle_hide
});

var button = require("sdk/ui/button/toggle").ToggleButton({
    label: "Manga List",
    id: "manga-list",
    icon: {
      "16": data.url("icon.png"),
    },
    onChange: function(state) {
        if (state.checked) {
            mangas.show({
              position: button
            });
        }
    }
  });

function handle_hide() {
    button.state('window', {checked: false});
}

var ss = require("sdk/simple-storage");
if (!ss.storage.info) {
  ss.storage.info = Object.create(null);
  ss.storage.updated = 0;
}

var pending = 0;
var delay = 300;
var max_retry = 5;

function update_info(info, mangasite, retry=0) {
    if (info.hold) {
        mangas.port.emit("set_info", info);
        return;
    }

    if (mangasite === undefined) {
        for (urlbase in mangasites) {
            if (info.last_uri.toLowerCase().startsWith(urlbase)) {
                mangasite = mangasites[urlbase];
                break;
            }
        }
    }
    if (mangasite === undefined)
        return;

    /* Don't update too often */
    if ((retry==0) && (info.updated && Date.now() - info.updated < 60))
        return;

    /* conservative value so that throttling works when updating */
    info.updated = Date.now();
    request.Request({
        url: info.last_uri,
        onComplete: function (response) {
            info.updated = Date.now(); /* more accurate update time */
            if (response.status != 200 || response.text.trim() == "") {
                /* Retry, with a delay so servers are not overloaded */
                if (retry < max_retry) {
                    pending = pending + 1;
                    setTimeout(function() {
                        pending = pending - 1;
                        update_info(info, mangasite, retry+1);
                    }, delay * pending);
                } else {
                    info.completed = false;
                    info.released = false;
                    info.error = true;
                }
            } else {
                info.completed = false;
                info.released = false;
                info.error = false;
                if (mangasite.completed_test(response.text)) {
                    info.completed = true;
                } else if (mangasite.released_test(response.text)) {
                    info.released = true;
                }
                mangas.port.emit("set_info", info);
            }
        }
    }).get();
}

function maybe_handle_manga(page, catchup=false) {
    var mangasite;
    for (urlbase in mangasites) {
        if (page.url.toLowerCase().startsWith(urlbase)) {
            mangasite = mangasites[urlbase];
            break;
        }
    }
    if (mangasite === undefined)
        return;

    var match = mangasite.extract(page.title);
    if(!match)
        return;

    name = match.name;
    var info = ss.storage.info[name];
    if (info === undefined) {
        info = Object.create(null);
        info.name = name;
        info.last_read = -1;
        ss.storage.info[name] = info;
    }
    info.hold = false; /* visited means no longer held */

    var current = match.current;
    var pagenum = match.pagenum;
    if (current > info.last_read) {
        info.last_read = current;
        info.last_page = pagenum;
        info.last_uri = page.url;
        update_info(info, mangasite);
    } else if (current == info.last_read && !(pagenum <= info.last_page)) {
        info.last_page = pagenum;
        info.last_uri = page.url;
        mangas.port.emit("set_info", info);
    }

    if (catchup)
        catchup_history();
}

function catchup_history() {
    var now = Date.now();
    var before = ss.storage.updated;
    ss.storage.updated = now;
    /* If the previous catchup time was less than 60 seconds ago, we assume that
     * the user is currently using Firefox to read mangas, so there is nothing
     * to catchup because of the ontabready hook. If not, maybe another firefox
     * was used in the mean time to read mangas, so we scan the history to find
     * out. The 60s delay is to prevent a history search every time the user
     * reads a page. */
    if (now - before < 60)
        return;
    for (urlbase in mangasites) {
        history.search(
            { url: urlbase + "*",
              from: before },
            { sort: "date", descending: true }
        ).on("data", maybe_handle_manga);
    }
}

tabs.on("ready", function(page) {
    maybe_handle_manga(page, true);
});

mangas.on("show", function () {
    var browser = viewFor(windows.activeWindow);
    mangas.resize(browser.innerWidth * 0.3, browser.innerHeight * 0.8);
    for (name in ss.storage.info) {
        update_info(ss.storage.info[name]);
    }
    catchup_history();
});

mangas.port.on("forget", function (name) {
    var info = ss.storage.info[name];
    /* catchup now so that the manga stays frozen even if it has
     * been visited since the last catchup */
    catchup_history();
    if (!info.hold) {
        info.hold = true;
        update_info(info);
    } else {
        delete ss.storage.info[name];
        ss.storage.updated = Date.now();
    }
});

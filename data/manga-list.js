var idfromname = Object.create(null);
var maxid = 0;

addon.port.on("set_info", function (info) {
    var div = null;
    var children = null;
    var manga = null;
    var link = null;
    var button = null;
    var i = 0;

    var id = idfromname[info.name];
    if (id === undefined) {
        manga = document.createElement("div");
        manga.id = maxid++;
        idfromname[info.name] = manga.id;

        link = document.createElement("a");
        link.target = "_blank";

        button = document.createElement("button");

        manga.appendChild(button);
        manga.appendChild(link);
    } else {
        manga = document.getElementById(id);
        button = manga.firstChild;
        link = manga.lastChild;
    }

    link.href = info.last_uri;
    link.textContent = info.name + " " + info.last_read + " p" + (info.last_page || 1);
    link.className = "";
    button.disabled = false;
    button.onclick = function () {
        if (info.hold) {
            link.className = "deleted";
            button.disabled = true;
        }
        addon.port.emit("forget", info.name);
    };

    if (info.hold) {
        button.textContent = "X";
        button.title = 'Completery forget "' + info.name + '"';
        div = document.getElementById('hold');
    } else {
        button.textContent = "H";
        button.title = 'Ignore updates to "' + info.name + '"';
        if (info.error)
            div = document.getElementById('error')
        else if (info.completed)
            div = document.getElementById('completed')
        else if (info.released)
            div = document.getElementById('mangas')
        else
            div = document.getElementById('unreleased');
    }

    if (manga.parentNode == div)
        return;

    children = div.childNodes
    for(i=0; i < children.length; i++) {
        if (manga.lastChild.textContent.localeCompare(
                    children[i].lastChild.textContent) < 0) {
            div.insertBefore(manga, children[i]);
            break;
        }
    }
    if (i == children.length)
        div.appendChild(manga);
});

